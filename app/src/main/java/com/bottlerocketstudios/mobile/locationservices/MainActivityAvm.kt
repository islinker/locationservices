package com.bottlerocketstudios.mobile.locationservices

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task

class MainActivityAvm: ViewModel() {

    private val checkForLocationPermission = MutableLiveData<Boolean>()
    private val checkIfShouldShowRationale = MutableLiveData<Boolean>()
    private val showRationale = MutableLiveData<DialogViewModel>()
    private val requestPermissions = MutableLiveData<Boolean>()
    private val playServicesError = MutableLiveData<Int>()
    private val checkLocationSettings = MutableLiveData<LocationSettingsRequest>()
    private val handleSettingsClientException = MutableLiveData<ResolvableApiException>()
    private val requestLocationUpdates = MutableLiveData<LocationUpdateInfo>()
    private val present = MutableLiveData<MainViewModel>()

    private val locationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.let { handleLocationResult(it) }
        }
    }

    private var currentLatitude = "???"
    private var currentLongitude = "???"
    private var locationEnabled = false
    private var requestingUpdates = false

    init {
        present.postValue(getMainViewModel())
    }

    fun checkGoogleApiAvailability(availability: Int) {
        when (availability) {
            ConnectionResult.SUCCESS -> checkForLocationPermission.postValue(true)
            else -> playServicesError.postValue(availability)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PLAY_SERVICES_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    checkForLocationPermission.postValue(true)
                } else {
                    locationEnabled = false
                    present.postValue(getMainViewModel())
                }
            }
            CHECK_SETTINGS_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    locationEnabled = true
                    if (requestingUpdates) {
                        requestUpdates()
                    } else {
                        present.postValue(getMainViewModel())
                    }
                } else {
                    locationEnabled = false
                    present.postValue(getMainViewModel())
                }
            }
        }
    }

    fun getLocationCallback() = locationCallback

    fun onPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MainActivityAvm.LOCATION_PERMISSION_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkLocationSettings.postValue(getLocationSettingsRequest())
                } else {
                    locationEnabled = false
                    present.postValue(getMainViewModel())
                }
                return
            }
        }
    }

    fun checkLocationPermissionStatus(permissionStatus: Int) {
        when (permissionStatus) {
            PackageManager.PERMISSION_GRANTED -> checkLocationSettings.postValue(getLocationSettingsRequest())
            PackageManager.PERMISSION_DENIED -> checkIfShouldShowRationale.postValue(true)
        }
    }

    fun handleShouldShowRationale(shouldShow: Boolean) {
        if (shouldShow) {
            showRationale.postValue(DialogViewModel("Location Services", "We needs them.", "Yes", "No"))
        } else {
            requestPermissions.postValue(true)
        }
    }

    fun rationaleAccepted() {
        requestPermissions.postValue(true)
    }

    fun rationaleDeclined() {
        locationEnabled = false
        present.postValue(getMainViewModel())
    }

    fun handleSettingsClientTask(task: Task<LocationSettingsResponse>) {
        task.addOnSuccessListener {
            locationEnabled = true
            if (requestingUpdates) {
                requestUpdates()
            } else {
                present.postValue(getMainViewModel())
            }
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                handleSettingsClientException.postValue(exception)
            } else {
                locationEnabled = false
                present.postValue(getMainViewModel())
            }
        }
    }

    fun handleLastKnownLocationTask(task: Task<Location>) {
        task.addOnSuccessListener { location : Location? ->
            if (location != null) {
                currentLatitude = location.latitude.toString()
                currentLongitude = location.longitude.toString()
            }

            present.postValue(getMainViewModel())
        }.addOnFailureListener {
            present.postValue(getMainViewModel())
        }
    }

    fun onLocationUpdatesClicked() {
        if (requestingUpdates) {
            stopLocationUpdates()
        } else {
            requestUpdates()
        }
    }

    private fun requestUpdates() {
        requestingUpdates = true
        val info = LocationUpdateInfo(requestingUpdates, getLocationRequest(), locationCallback)
        requestLocationUpdates.postValue(info)
        present.postValue(getMainViewModel())
    }

    private fun stopLocationUpdates() {
        requestingUpdates = false
        val info = LocationUpdateInfo(requestingUpdates, getLocationRequest(), locationCallback)
        requestLocationUpdates.postValue(info)
        present.postValue(getMainViewModel())
    }

    private fun getMainViewModel(): MainViewModel {
        return MainViewModel(
                locationEnabled,
                "Latitude: $currentLatitude",
                "Longitude: $currentLongitude",
                requestingUpdates
        )
    }

    private fun getLocationSettingsRequest(): LocationSettingsRequest {
        val locationRequest = getLocationRequest()
        return LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .build()
    }

    private fun getLocationRequest(): LocationRequest {
        return LocationRequest().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private fun handleLocationResult(result: LocationResult) {
        val locations = result.locations.filterNotNull()
        if (locations.isEmpty()) {
            present.postValue(getMainViewModel())
        } else {
            val latest = locations.sortedByDescending { it.elapsedRealtimeNanos }.first()
            currentLatitude = latest.latitude.toString()
            currentLongitude = latest.longitude.toString()
            present.postValue(getMainViewModel())
        }
    }

    fun checkForLocationPermission(): LiveData<Boolean> = checkForLocationPermission
    fun checkIfShouldShowRationale(): LiveData<Boolean> = checkIfShouldShowRationale
    fun showRationale(): LiveData<DialogViewModel> = showRationale
    fun requestPermissions(): LiveData<Boolean> = requestPermissions
    fun checkForLocationSettings(): LiveData<LocationSettingsRequest> = checkLocationSettings
    fun playServicesError(): LiveData<Int> = playServicesError
    fun handleSettingsClientException(): LiveData<ResolvableApiException> = handleSettingsClientException
    fun requestLocationUpdates(): LiveData<LocationUpdateInfo> = requestLocationUpdates
    fun present(): LiveData<MainViewModel> = present

    companion object {
        const val PLAY_SERVICES_REQUEST = 1221
        const val LOCATION_PERMISSION_REQUEST = 1222
        const val CHECK_SETTINGS_REQUEST = 1223
    }

}