package com.bottlerocketstudios.mobile.locationservices

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*

class MainActivity : AppCompatActivity(), MainPresenter.Listener {

    private lateinit var avm: MainActivityAvm
    private lateinit var container: MainPresenter.Container
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val root: View = findViewById(R.id.main_activity_root)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        container = MainPresenter.Container(root, this)
        avm = ViewModelProviders.of(this).get(MainActivityAvm::class.java)

        avm.playServicesError().observe(this, Observer { errorCode ->
            errorCode?.let {
                GoogleApiAvailability.getInstance().showErrorDialogFragment(this, it, MainActivityAvm.PLAY_SERVICES_REQUEST)
            }
        })

        avm.checkForLocationPermission().observe(this, Observer {
            if (it == true) { checkForLocationPermissions() }
        })

        avm.checkIfShouldShowRationale().observe(this, Observer {
            if (it == true) shouldShowRationale()
        })

        avm.showRationale().observe(this, Observer { vm ->
            vm?.let { showRationale(it) }
        })

        avm.requestPermissions().observe(this, Observer {
            if (it == true) { requestPermission() }
        })

        avm.checkForLocationSettings().observe(this, Observer { request ->
            request?.let { checkForLocationSettings(it) }
        })

        avm.handleSettingsClientException().observe(this, Observer { exception ->
            exception?.let { handleSettingsClientException(it) }
        })

        avm.present().observe(this, Observer { vm ->
            vm?.let {
                MainPresenter.present(container, it)
            }
        })

        avm.requestLocationUpdates().observe(this, Observer { info ->
            info?.let { requestLocationUpdates(it) }
        })
    }

    override fun onLastKnownLocationClicked() {
        getLastKnownLocation()
    }

    override fun onLocationUpdatesClicked() {
        avm.onLocationUpdatesClicked()
    }

    override fun onStart() {
        super.onStart()

        val apiAvailability = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        avm.checkGoogleApiAvailability(apiAvailability)
    }

    override fun onStop() {
        super.onStop()

        fusedLocationClient.removeLocationUpdates(avm.getLocationCallback())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        avm.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        avm.onPermissionResult(requestCode, permissions, grantResults)
    }

    private fun checkForLocationPermissions() {
        val fineLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        avm.checkLocationPermissionStatus(fineLocationPermission)
    }

    private fun shouldShowRationale() {
        val shouldShowRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
        avm.handleShouldShowRationale(shouldShowRationale)
    }

    private fun showRationale(vm: DialogViewModel) {
        val builder = AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert)
        vm.title?.let {
            builder.setTitle(it)
        }
        builder.setMessage(vm.message)
        builder.setPositiveButton(vm.positiveButtonText) { _, _ ->
            avm.rationaleAccepted()
        }
        vm.negativeButtonText?.let {
            builder.setNegativeButton(it) { _, _ ->
                avm.rationaleDeclined()
            }
        }
        builder.show()
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MainActivityAvm.LOCATION_PERMISSION_REQUEST)
    }

    private fun checkForLocationSettings(request: LocationSettingsRequest) {
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(request)
        avm.handleSettingsClientTask(task)
    }

    private fun handleSettingsClientException(exception: ResolvableApiException) {
        try {
            exception.startResolutionForResult(this, MainActivityAvm.CHECK_SETTINGS_REQUEST)
        } catch (sendEx: IntentSender.SendIntentException) {
            // Ignore the error.
            getLastKnownLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        avm.handleLastKnownLocationTask(fusedLocationClient.lastLocation)
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates(info: LocationUpdateInfo) {
        if (info.requestingUpdates) {
            fusedLocationClient.requestLocationUpdates(
                    info.request,
                    info.callback,
                    null /* Looper */)
        } else {
            fusedLocationClient.removeLocationUpdates(info.callback)
        }
    }

}
