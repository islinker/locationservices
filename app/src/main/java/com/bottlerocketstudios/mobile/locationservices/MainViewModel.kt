package com.bottlerocketstudios.mobile.locationservices

data class MainViewModel(
        val locationEnabled: Boolean,
        val currentLatitude: String?,
        val currentLongitude: String?,
        val requestingUpdates: Boolean)