package com.bottlerocketstudios.mobile.locationservices

import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest

class LocationUpdateInfo(
        val requestingUpdates: Boolean,
        val request: LocationRequest,
        val callback: LocationCallback
)