package com.bottlerocketstudios.mobile.locationservices

data class DialogViewModel(
        val title: String?,
        val message: String,
        val positiveButtonText: String,
        val negativeButtonText: String?
)