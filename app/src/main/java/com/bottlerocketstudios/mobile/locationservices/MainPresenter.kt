package com.bottlerocketstudios.mobile.locationservices

import android.view.View
import android.widget.Button
import android.widget.TextView

class MainPresenter {

    class Container(root: View, listener: Listener) {
        val currentLatitude: TextView = root.findViewById(R.id.current_location_latitude)
        val currentLongitude: TextView = root.findViewById(R.id.current_location_longitude)
        val lastKnownLocationButton: Button = root.findViewById(R.id.last_known_location)
        val locationUpdatesButton: Button = root.findViewById(R.id.location_updates)

        init {
            lastKnownLocationButton.setOnClickListener {
                listener.onLastKnownLocationClicked()
            }
            locationUpdatesButton.setOnClickListener {
                listener.onLocationUpdatesClicked()
            }
        }
    }

    interface Listener {
        fun onLastKnownLocationClicked()
        fun onLocationUpdatesClicked()
    }

    companion object {
        fun present(container: Container, viewModel: MainViewModel) {
            container.currentLatitude.text = viewModel.currentLatitude
            container.currentLongitude.text = viewModel.currentLongitude
            container.lastKnownLocationButton.isEnabled = viewModel.locationEnabled
            container.locationUpdatesButton.isEnabled = viewModel.locationEnabled
            container.locationUpdatesButton.text = if (viewModel.requestingUpdates) "Stop Location Updates" else "Request Location Updates"
        }
    }
}